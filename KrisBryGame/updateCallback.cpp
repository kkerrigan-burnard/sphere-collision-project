#include <cEngine.h>
#include <cConsole.h>
#include <cDebugRenderer.h>

#include <cSceneManager.h>
#include <cShaderManager.h>

#include <cTransformComponent.h>
#include <cRenderMeshComponent.h>

#include <cMeshRenderSystem.h>

#include <physicsShapes.h>
#include <cColliderSystem.h>

#include <iInputCommand.h>


extern cEntity* pSphere;
extern cEntity* pCube;

extern cAABBTerrianBroadPhaseHierarchy g_AABBTerrianHierarchy;

double nextLightSwitchTime = 0;

void showModelNormals();

float randomFloat(float a, float b) {
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = b - a;
	float r = random * diff;
	return a + r;
}

void colourSelectedRigidBody(cEntity* entity) {
	
	cScene* scene = cSceneManager::getInstance()->getActiveScene();

	//get the selected entity and the component we need
	cEntity* pEntity = scene->getSelectedEntity();

	cRenderMeshComponent* pMeshComp = static_cast<cRenderMeshComponent*>(entity->components[3]);
	if (!pMeshComp) return;

	if (entity == pEntity) {
		
		pMeshComp->setDiffuseColour(glm::vec3(1.0f, 0.0f, 0.0f));
	}
	else {
		pMeshComp->setDiffuseColour(glm::vec3(0.0f, 0.0f, 0.0f));
	}
}

void updateCallback(double deltaTime) {

	cShaderManager::getInstance()->useShaderProgram("BasicUberShader");
	GLint program = cShaderManager::getInstance()->getIDFromFriendlyName("BasicUberShader");

	cSceneManager* pSceneManager = cSceneManager::getInstance();
	cScene* pScene = pSceneManager->getActiveScene();

	// Change the colour of the selected entity
	std::vector<cEntity*> entities = cSceneManager::getInstance()->getActiveScene()->getEntities();
	for (size_t index = 0; index != entities.size(); ++index) {
		
		colourSelectedRigidBody(entities[index]);
	}

	// Follow the selected entity
	cEntity* pSelectedEntity = pScene->getSelectedEntity();
	pScene->flyCamera.followSelectedPhysicsEntity(deltaTime, pSelectedEntity);

	// Render debug spheres for lights?
	if (pScene->bIsLightDebug) {
		pScene->setLightDebugSphere(pSphere);
	}

	// Debug render
	if (pScene->bIsRenderDebug) {
		showModelNormals();
	}
}