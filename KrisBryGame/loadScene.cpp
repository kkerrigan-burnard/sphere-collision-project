#include <cVAOMeshManager.h>
#include <cEntityBuilder.h>
#include <cSceneManager.h>
#include <cLightManager.h>
#include <cSoundManager.h>
#include <cLuaBrain.h>
#include <cScriptingCommandSystem.h>
#include <cColliderSystem.h>
#include <cSerialization.h>
#include <fstream>

cEntity* pSphere = nullptr;
cEntity* pCube = nullptr;

sLight* pMovingSpotLight = nullptr;

extern std::map<std::string, cCommandGroup*> commandGroups;
extern std::map<std::string, iCommand*> commands;
extern cAABBTerrianBroadPhaseHierarchy g_AABBTerrianHierarchy;

void loadScene() {

	// create a scene for the entities
	cSceneManager* pSceneManager = cSceneManager::getInstance();
	cScene* pSandboxScene = pSceneManager->createScene("sandbox.json");
	pSceneManager->setActiveScene("sandbox.json");

	//initialize the light manager before loading them
	cLightManager::getInstance()->initializeLights();

	pSphere = cEntityBuilder::getInstance()->createEntity(0);
	static_cast<cRenderMeshComponent*>(pSphere->components[3])->bIsVisible = false;
	pCube = cEntityBuilder::getInstance()->createEntity(12);

	cColliderSystem::getInstance()->pColliderSphere = pSphere;
	cColliderSystem::getInstance()->pAABBCube = pCube;

	//load from the file
	cSerialization::deserializeSceneCamera("cameras.json");
	cSerialization::deserializeSceneLights("lights.json");
	cSerialization::deserializeSceneSounds("sounds.json");
	cSerialization::deserializeSceneEntities("entities.json");

	cEntityBuilder* pBuilder = cEntityBuilder::getInstance();

	return;
}