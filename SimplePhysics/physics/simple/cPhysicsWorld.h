/**
 * @file
 * @author  Kristian Kerrigan <k_kerrigan3@fanshaweonline.ca>
 * @version 1.0
 *
 * @section DESCRIPTION
 *
 * Enter a short description.
 */

#ifndef _cPhysicsWorld_HG_
#define _cPhysicsWorld_HG_

#include <vector>

#include <physics/interfaces/iPhysicsWorld.h>
#include <physics/interfaces/iRigidBody.h>

#include "sRK4State.h"
#include "sCollisionDetails.h";

namespace nPhysics {

	class cPhysicsWorld : public iPhysicsWorld {
	public:

		virtual ~cPhysicsWorld();

		virtual void setGravity(const glm::vec3& grav) override;

		virtual bool addRigidBody(iRigidBody* pBody) override;
		virtual bool removeRigidBody(iRigidBody* pBody) override;

		virtual void update(float deltaTime) override;

	private:
		glm::vec3 mGravity;
		float mElaspedTime = 0.0f;
		std::vector<iRigidBody*> mBodies;

		bool isColliding(iRigidBody* bodyA, iRigidBody* bodyB, sCollisionDetails& collision, float deltaTime);
		int intersectMovingSpherePlane(iRigidBody* sphere, glm::vec3 movementVector, iRigidBody* plane, float &acceleration, glm::vec3& q) const;
		int intersectMovingSphereSphere(iRigidBody* sphereA, iRigidBody* sphereB, float& acceleration) const;

		// RK4 Integration
		Derivative evaluate(const State& initialState, glm::vec3 t, float deltaTime, const Derivative& d);
		void integrate(State& state, glm::vec3 t, float deltaTime);

		// Collision response
		void spherePlaneCollisionResponse(sCollisionDetails& collision, float deltaTime);
		void sphereSphereCollisionResponse(sCollisionDetails& collision, float deltaTime);
	};
}

#endif
