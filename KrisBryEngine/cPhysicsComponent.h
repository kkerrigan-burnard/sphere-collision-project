/**
 * @file
 * @author  Kristian Kerrigan <k_kerrigan3@fanshaweonline.ca>
 * @version 1.0
 *
 * @section DESCRIPTION
 *
 * Enter a short description.
 */

#ifndef _cPhysicsComponent_HG_
#define _cPhysicsComponent_HG_

#include <physics/interfaces/iRigidBody.h>
#include <physics/interfaces/sRigidBodyDef.h>

#include "cComponent.h"

#include <rapidjson/prettywriter.h>

class cPhysicsComponent : public cComponent {
public:
	cPhysicsComponent();
	virtual ~cPhysicsComponent();

	nPhysics::iRigidBody* getRigidBody();
	void setRigidBody(nPhysics::iRigidBody* pBody);

	nPhysics::iShape* getShape();

	void serializeComponent(rapidjson::PrettyWriter<rapidjson::StringBuffer>& writer);

private:
	nPhysics::iRigidBody* mRigidBody;
};

#endif
