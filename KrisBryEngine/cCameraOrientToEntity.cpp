#include "cCameraOrientToEntity.h"
#include "cTransformComponent.h"

cCameraOrientToEntity::cCameraOrientToEntity(cFlyCamera* camera, cEntity* entityTarget, float duration, float easeIn, float easeOut): camera(camera),
	entityTarget(entityTarget), firstUpdateDone(false),
	initialTime(0),
	elapsedTime(0), duration(duration),
	finished(false){}

void cCameraOrientToEntity::update(float deltaTime) {
	
	if (!this->firstUpdateDone) {
		
		this->initialTime = glfwGetTime();
		this->elapsedTime = 0.0f;

		initialPosition = static_cast<cTransformComponent*>(this->entityTarget->components[1])->position;
		lookAtPosition = initialPosition;
		theUpVector = glm::vec3(0.0f, 1.0f, 0.0f);
		initialOrientation = glm::toMat4(this->camera->getQOrientation());
	}

	glm::vec3 direction = glm::normalize(((cTransformComponent*)this->entityTarget->components[1])->position - this->camera->eye);

	//rotation
	this->finalOrientation = glm::toMat4(glm::quatLookAt(-direction, glm::vec3(0.0f, 1.0f, 0.0f)));

	this->firstUpdateDone = true;

	cTransformComponent* targetTransform = static_cast<cTransformComponent*>(this->entityTarget->components[1]);


	// Transform the Orientation Matrix to Quaternion
	glm::quat quatStart = glm::quat_cast(this->initialOrientation);
	glm::quat quatEnd = glm::quat_cast(this->finalOrientation);

	this->elapsedTime = glfwGetTime() - this->initialTime;
	float factor = this->elapsedTime / this->duration;

	if(factor > 1.0f) {
		factor = 1.0f;
	}

	// Transform the resulting quaternion back to the Matrix
	// NOTE: No slerp done in this command because we always want to be snapped to
	// the selected entity.
	this->camera->setMeshOrientationQ(glm::quat(finalOrientation));

	//are we done?
	if (this->elapsedTime >= this->duration) {
		this->finished = true;
	}
}

bool cCameraOrientToEntity::isFinished() {
	return this->finished;
}
