#include "cEntityBuilder.h"
#include "cComponentFactory.h"

#include "cEulerMovementSystem.h"
#include "cMeshRenderSystem.h"
#include "cColliderSystem.h"

#include "cRenderMeshComponent.h"
#include "cTransformComponent.h"
#include "cEulerMovementComponent.h"
#include "cCollider.h"

#include "physicsShapes.h"
#include "cShaderManager.h"
#include "cLightManager.h"
#include "cSceneManager.h"
#include "c3dSoundComponent.h"
#include "cSoundManager.h"

void ballDebugRendering(cEntity* entity);

cEntityBuilder::cEntityBuilder(){
}


cEntityBuilder::~cEntityBuilder(){
	for (cEntity* entity : this->allocatedEntites) {
		delete entity;
	}
}

cEntityBuilder* cEntityBuilder::getInstance() {
	static cEntityBuilder instance;

	return &instance;
}

cEntity* cEntityBuilder::createEntity(int entityType) {
	cEntity* result = nullptr;

	switch (entityType) {
	
		//allowed entity types
		case 0: // sphere
		case 1: // Sky box object
		case 2: // Island 200
		case 3: // Cube
		case 4: // Rectangle
			result = new cEntity(entityType);
			break;
	}

	if (result != nullptr) {
		// add the components required
		addRequiredComponents(result, entityType);

		this->allocatedEntites.push_back(result);
	}

	return result;
}

cEntity* cEntityBuilder::createEntity(int entityType, glm::vec3 position) {
	cEntity* result = nullptr;

	result = this->createEntity(entityType);

	this->setEntityPosition(result, position);

	return result;
}

void cEntityBuilder::setEntityPosition(cEntity* entity, glm::vec3 position) {

	cTransformComponent* transformComponent = (cTransformComponent*) entity->components[1];
	transformComponent->position = position;
}

cEntity* cEntityBuilder::createEntityRandomPosColor(int entityType) {
	cEntity* result = this->createEntity(entityType);

	// make sure we have the compoents required
	if ((result->componentBitField & 5) != 5) {
		return result;
	}

	cTransformComponent* pTransformComponent =static_cast<cTransformComponent*>(result->components[1]);
	pTransformComponent->position = glm::vec3(this->getRandInRange<float>(-75.0f, 75.0f), this->getRandInRange<float>(-75.0f, 75.0f), this->getRandInRange<float>(-75.0f, 75.0f));

	cRenderMeshComponent* pRenderMeshComponent = static_cast<cRenderMeshComponent*>(result->components[3]);
	pRenderMeshComponent->materialDiffuse = glm::vec4(this->getRandBetween0and1<float>(), this->getRandBetween0and1<float>(), this->getRandBetween0and1<float>() , 1.0f);
	return result;
}

void cEntityBuilder::addRequiredComponents(cEntity* entity, int entityType) {

	cComponentFactory* pComponentFactory = cComponentFactory::getInstance();

	switch (entityType)
	{
		case 0:
		{
			entity->componentBitField |= 1;
			entity->components[1] = pComponentFactory->createComponent(1);
	
			entity->componentBitField |= 2;
			entity->components[2] = pComponentFactory->createComponent(2);

			entity->componentBitField |= 4;
			entity->components[3] = pComponentFactory->createComponent(3);
			{
				cRenderMeshComponent* meshComp = static_cast<cRenderMeshComponent*>(entity->components[3]);
				meshComp->meshName = "Sphere_320_faces_xyz_n_GARBAGE_uv.ply";
				meshComp->friendlyName = "Sphere";
				meshComp->bIsVisible = true;
				meshComp->bIsWireFrame = true;
				meshComp->materialDiffuse = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
				meshComp->bUseVertexColour = false;
				meshComp->bDontLight = false;
				meshComp->shaderId = cShaderManager::getInstance()->getIDFromFriendlyName("BasicUberShader");
			}

			entity->components[4] = pComponentFactory->createComponent(4);

			// register the entity with the component systems
			cMeshRenderSystem::getInstance()->registerEntity(entity);
		}
		break;

		case 1:
		{
			entity->componentBitField |= 1;
			entity->components[1] = pComponentFactory->createComponent(1);

			cTransformComponent* pTransformComponent = static_cast<cTransformComponent*>(entity->components[1]);
			pTransformComponent->position = glm::vec3(0.0f, 0.0f, 0.0f);
			pTransformComponent->scale = glm::vec3(5000.0f);


			entity->componentBitField |= 4;
			entity->components[3] = pComponentFactory->createComponent(3);
			{
				cRenderMeshComponent* meshComp = static_cast<cRenderMeshComponent*>(entity->components[3]);
				meshComp->meshName = "Sphere_320_faces_xyz_n_GARBAGE_uv_INVERTED_NORMALS.ply";
				meshComp->friendlyName = "SkyBoxObject";
				meshComp->bIsVisible = true;
				meshComp->materialDiffuse = glm::vec4(1.0f, 105.0f / 255.0f, 180.0f / 255.0f, 1.0f);
				meshComp->bUseVertexColour = false;
				meshComp->bDontLight = false;
				meshComp->shaderId = cShaderManager::getInstance()->getIDFromFriendlyName("BasicUberShader");
			}

			// register the entity with the component systems
			cMeshRenderSystem::getInstance()->registerEntity(entity);
		}
		break;

		case 2:
		{
			entity->componentBitField |= 1;
			entity->components[1] = pComponentFactory->createComponent(1);

			entity->componentBitField |= 2;
			entity->components[2] = pComponentFactory->createComponent(2);

			entity->componentBitField |= 4;
			entity->components[3] = pComponentFactory->createComponent(3);
			{
				cRenderMeshComponent* meshComp = static_cast<cRenderMeshComponent*>(entity->components[3]);
				meshComp->meshName = "flat.ply";
				meshComp->friendlyName = "Plane";
				meshComp->bIsVisible = true;
				meshComp->bIsWireFrame = true;
				meshComp->materialDiffuse = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
				meshComp->bUseVertexColour = false;
				meshComp->bDontLight = false;
				meshComp->shaderId = cShaderManager::getInstance()->getIDFromFriendlyName("BasicUberShader");
			}

			entity->components[4] = pComponentFactory->createComponent(4);

			// register the entity with the component systems
			cMeshRenderSystem::getInstance()->registerEntity(entity);
			
		}
		break;	

		case 3:
		{
			entity->componentBitField |= 1;
			entity->components[1] = pComponentFactory->createComponent(1);

			cTransformComponent* pTransformComponent = static_cast<cTransformComponent*>(entity->components[1]);
			pTransformComponent->position = glm::vec3(0.0f, 2.0f, 0.0f);
			pTransformComponent->scale = glm::vec3(10.0f);

			entity->components[2] = pComponentFactory->createComponent(2);
			entity->componentBitField |= 4;
			entity->components[3] = pComponentFactory->createComponent(3);
			{
				cRenderMeshComponent* meshComp = static_cast<cRenderMeshComponent*>(entity->components[3]);
				meshComp->meshName = "cube_flat_shaded_xyz_n.ply";
				meshComp->bIsVisible = false;
				meshComp->bIsWireFrame = true;
				meshComp->materialDiffuse = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
				meshComp->bUseVertexColour = false;
				meshComp->bDontLight = true;
				meshComp->shaderId = cShaderManager::getInstance()->getIDFromFriendlyName("BasicUberShader");
			}

			// register the entity with the component systems
			cMeshRenderSystem::getInstance()->registerEntity(entity);
		}
		break;

		case 4:
		{
			entity->componentBitField |= 1;
			entity->components[1] = pComponentFactory->createComponent(1);

			cTransformComponent* pTransformComponent = static_cast<cTransformComponent*>(entity->components[1]);
			pTransformComponent->position = glm::vec3(0.0f, 0.0f, 0.0f);
			pTransformComponent->scale = glm::vec3(1.0f);

			entity->componentBitField |= 4;
			entity->components[3] = pComponentFactory->createComponent(3);
			{
				cRenderMeshComponent* meshComp = static_cast<cRenderMeshComponent*>(entity->components[3]);
				meshComp->meshName = "rect1_xyz_n_uv.ply";
				meshComp->friendlyName = "rec1";
				meshComp->bIsVisible = true;
				meshComp->bIsWireFrame = true;
				meshComp->materialDiffuse = glm::vec4(1.0f);
				meshComp->bUseVertexColour = false;
				meshComp->bDontLight = false;
				meshComp->shaderId = cShaderManager::getInstance()->getIDFromFriendlyName("BasicUberShader");
			}

			// register the entity with the component systems
			cMeshRenderSystem::getInstance()->registerEntity(entity);
		}
		break;
	}

	
}

template<typename T>
T cEntityBuilder::getRandBetween0and1(void)
{
	return (T)((double)rand() / (RAND_MAX));
}

template <class T>
T cEntityBuilder::getRandInRange(T min, T max)
{
	double value =
		min + static_cast <double> (rand())
		/ (static_cast <double> (RAND_MAX / (static_cast<double>(max - min))));
	return static_cast<T>(value);
}