/**
 * @file
 * @author  Kristian Kerrigan <k_kerrigan3@fanshaweonline.ca>
 *
 * @section DESCRIPTION
 *
 * Enter a short description.
 */

#include "cPhysicsComponent.h"

cPhysicsComponent::cPhysicsComponent() : cComponent(4) {

	this->mRigidBody = nullptr;

	return;
}

cPhysicsComponent::~cPhysicsComponent() {

	delete this->mRigidBody;
	this->mRigidBody = nullptr;

	return;
}

nPhysics::iRigidBody* cPhysicsComponent::getRigidBody() {

	return this->mRigidBody;
}

void cPhysicsComponent::setRigidBody(nPhysics::iRigidBody* pBody) {

	this->mRigidBody = pBody;
}

nPhysics::iShape* cPhysicsComponent::getShape() {

	return this->mRigidBody->getShape();
}

void cPhysicsComponent::serializeComponent(rapidjson::PrettyWriter<rapidjson::StringBuffer>& writer) {

	//Start Component
	writer.StartObject();

	writer.String("componentType");
	writer.Int(this->COMPONENT_TYPE_ID);

	writer.String(("position"));
	writer.StartObject();
	
	glm::vec3 pos;
	this->mRigidBody->getPosition(pos);
	writer.String("x");
	writer.Double(pos.x);
	writer.String("y");
	writer.Double(pos.y);
	writer.String("z");
	writer.Double(pos.z);
	writer.EndObject();

	writer.String(("orientation"));
	writer.StartObject();

	glm::vec3 ori;
	this->mRigidBody->getOrientation(ori);
	writer.String("x");
	writer.Double(ori.x);
	writer.String("y");
	writer.Double(ori.y);
	writer.String("z");
	writer.Double(ori.z);
	writer.EndObject();

	writer.String(("acceleration"));
	writer.StartObject();

	glm::vec3 accel;
	this->mRigidBody->getAcceleration(accel);
	writer.String("x");
	writer.Double(accel.x);
	writer.String("y");
	writer.Double(accel.y);
	writer.String("z");
	writer.Double(accel.z);
	writer.EndObject();

	writer.String(("velocity"));
	writer.StartObject();

	glm::vec3 vel;
	this->mRigidBody->getVelocity(vel);
	writer.String("x");
	writer.Double(vel.x);
	writer.String("y");
	writer.Double(vel.y);
	writer.String("z");
	writer.Double(vel.z);
	writer.EndObject();


	return;
}